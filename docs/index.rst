.. niamoto documentation master file, created by
   sphinx-quickstart on Mon May  2 18:09:11 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to niamoto's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

    installation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

