niamoto Installation guide
===============================

niamoto is made of several components that together constitute a system.

As niamoto tries to emphasis encapsulation, most of the components are defined
inside Docker containers.


1- The PostgreSQL / PostGIS server
----------------------------------


2- The Geoserver
----------------


3- The RabbitMQ server
----------------------


4- The Django project
---------------------
